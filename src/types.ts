export type Value = number | string | Value[] | Dictionary
export type Dictionary = { [key: string]: Value }
