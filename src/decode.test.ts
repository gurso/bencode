import { assertEquals } from "https://deno.land/std@0.121.0/testing/asserts.ts"
import { decode, encode, Value } from "../mod.ts"

function enc(v: Value): ArrayBuffer {
  const s = encode(v)
  const u = new TextEncoder().encode(s)
  return u.buffer
}

Deno.test("decode interger : ", () => {
  assertEquals(decode(enc(42)), 42)
})

Deno.test("decode string : ", () => {
  const str = "anticonstitutionnellement"
  assertEquals(decode(enc(str)), str)
})

Deno.test("decode list : ", () => {
  const arr = [23, "test"]
  assertEquals(decode(enc(arr)), arr)
})

Deno.test("decode deep list : ", () => {
  const arr = [23, "bar", [12, "baz", [34, "foo"]]]
  assertEquals(decode(enc(arr)), arr)
})

Deno.test("decode deep dictionnary : ", () => {
  const deep = {
    foo: {
      bar: {
        baz: 23
      }
    }
  }
  assertEquals(decode(enc(deep)), deep)
})

Deno.test("Mix of all : ", () => {
  const deep = {
    foo: {
      bar: {
        baz: 23
      }
    }
  }
  assertEquals(decode(enc(deep)), deep)
})
