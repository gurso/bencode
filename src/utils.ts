import { Dictionary } from "./types.ts"

export function isDictionary(o: unknown): o is Dictionary {
  return o !== null && typeof o === "object"
}
