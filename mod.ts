export { encode } from "./src/encode.ts"
export { decode } from "./src/decode.ts"
export type { Value, Dictionary } from "./src/types.ts"
