import { Dictionary, Value } from "./types.ts"

function isNumber(bc: { u8: Uint8Array; i: number }): boolean {
  return bc.u8[bc.i] >= 48 && bc.u8[bc.i] <= 57
}

function spliceNumber(bc: { u8: Uint8Array; i: number }): number {
  const j = bc.i
  while (isNumber(bc)) {
    bc.i++
  }
  const u8n = bc.u8.slice(j, bc.i)
  const s = new TextDecoder().decode(u8n)
  return Number(s)
}

function decodeNumber(bc: { u8: Uint8Array; i: number }): number {
  bc.i++ // forward "i"
  const n = spliceNumber(bc)
  bc.i++ // forward "e"
  return n
}

function decodeString(bc: { u8: Uint8Array; i: number }): string {
  const l = spliceNumber(bc)
  bc.i++ // forward ":"
  const u8s = bc.u8.slice(bc.i, bc.i + l)
  const s = new TextDecoder().decode(u8s)
  bc.i += l // forward string content
  return s
}

function decodeList(bc: { u8: Uint8Array; i: number }): Value[] {
  bc.i++ // forward "l"
  const r: Value[] = []
  while (bc.u8[bc.i] !== "e".charCodeAt(0)) {
    r.push(_decode(bc))
  }
  bc.i++ // forward "e"
  return r
}

function decodeDictionary(bc: { u8: Uint8Array; i: number }): Dictionary {
  bc.i++ // forward "d"
  const r: Dictionary = {}
  let k: string | null = null
  while (bc.u8[bc.i] !== "e".charCodeAt(0)) {
    if (k) {
      r[k] = _decode(bc)
      k = null
    } else {
      k = decodeString(bc)
    }
  }
  bc.i++ // forward "e"
  return r
}

function _decode(bc: { u8: Uint8Array; i: number }): Value {
  if (bc.u8[bc.i] === "i".charCodeAt(0)) {
    return decodeNumber(bc)
  } else if (bc.u8[bc.i] === "l".charCodeAt(0)) {
    return decodeList(bc)
  } else if (bc.u8[bc.i] === "d".charCodeAt(0)) {
    return decodeDictionary(bc)
  } else if (isNumber(bc)) {
    return decodeString(bc)
  } else {
    throw new Error("Cannot decode bencode, type unknow.")
  }
}

export function decode(bencode: ArrayBuffer): Value {
  const u8 = new Uint8Array(bencode)
  const i = 0
  return _decode({ u8, i })
}
