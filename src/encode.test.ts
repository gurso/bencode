import { assertEquals } from "https://deno.land/std@0.121.0/testing/asserts.ts"
import { encode } from "../mod.ts"

Deno.test("encode interger : ", () => {
  assertEquals(encode(42), "i42e")
})

Deno.test("encode string : ", () => {
  assertEquals(
    encode("anticonstitutionnellement"),
    "25:anticonstitutionnellement"
  )
  assertEquals(encode("23test"), "6:23test")
})

Deno.test("encode list : ", () => {
  assertEquals(encode([42, "test", 23, "str"]), "li42e4:testi23e3:stre")
})
Deno.test("encode deep list : ", () => {
  assertEquals(encode([42, "test", 23, ["str"]]), "li42e4:testi23el3:stree")
})

Deno.test("encode dictionnary : ", () => {
  const bar = "rdmString"
  assertEquals(
    encode({
      foo: 234,
      bar
    }),
    `d${encode("foo")}${encode(234)}${encode("bar")}${encode(bar)}e`
  )
})

Deno.test("encode deep dictionnary : ", () => {
  const bar = "rdmString"
  assertEquals(
    encode({
      foo: 234,
      bar,
      deep: {
        baz: "so",
        arr: [23, "and"]
      }
    }),
    `d${encode("foo")}${encode(234)}${encode("bar")}${encode(bar)}${encode(
      "deep"
    )}d${encode("baz")}${encode("so")}${encode("arr")}${encode([23, "and"])}ee`
  )
})
