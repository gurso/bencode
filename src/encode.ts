import { Dictionary, Value } from "./types.ts"
import { isDictionary } from "./utils.ts"

function encodeInteger(n: number): string {
  return `i${n}e`
}

function encodeString(s: string): string {
  return `${s.length}:${s}`
}

function encodeList(arr: readonly Value[]): string {
  const content = arr.map(encode).join("")
  return "l" + content + "e"
}

function encodeDictionary(dictionary: Dictionary): string {
  let s = "d"
  for (const k in dictionary) {
    s += encodeString(k)
    s += encode(dictionary[k])
  }
  return s + "e"
}

export function encode(a: Value): string {
  if (Number.isInteger(a)) {
    return encodeInteger(a as number)
  } else if (Array.isArray(a)) {
    return encodeList(a)
  } else if (isDictionary(a)) {
    return encodeDictionary(a)
  } else if (typeof a === "string") {
    return encodeString(a as string)
  } else {
    throw new Error("Cannot encode value, type unknown.")
  }
}
